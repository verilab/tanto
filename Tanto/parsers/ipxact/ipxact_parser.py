'''
Created on Jun 28, 2012

Copyright 2012 Verilab, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
   
@author: gmcgregor
'''
#try:
#    from lxml import etree as ET
#    using_lxml=True
#except:
#    print 'Failed to load lxml, falling back to basic xml.etree support. See http://lxml.de/installation.html for lxml installation details'
#    using_lxml=False
   
from xml.etree import ElementTree as ET
from logging import info, debug, warning, error, critical, exception
from tanto.tanto import TantoParser
import sys
import string

from genxmlif import GenXmlIfError
from minixsv import pyxsval 

class IpxactParser(TantoParser):
    
    def parse(self):
        '''IP-XACT parsing happens here'''
        debug('Parsing IP-XACT files, starting with: %s'% self.parent.config.get('tanto', 'top'))
        
        # Parse the XML/ SPIRIT description
        try:
            filename = self.parent.config.get('tanto', 'source_directory') + \
                self.parent.config.get('tanto', 'top') + \
                '.' + self.parent.config.get('ipxact', 'ipxact_extension')
            
            if eval(self.parent.config.get('ipxact', 'validate_against_schema')):
                elementTreeWrapper = pyxsval.parseAndValidateXmlInput(filename, xmlIfClass = pyxsval.XMLIF_ELEMENTTREE, errorLimit=200, verbose=1, useCaching=1, processXInclude=0)
                self.root = elementTreeWrapper.getTree().getroot()  
            else:
                self.root = ET.parse(filename).getroot()
                
        except pyxsval.XsvalError, errstr:
            print 'IP-XACT error validating %s :: %s' % (filename, errstr)
            critical("Validation aborted!")
            if self.parent.args.debug:
                raise pyxsval.XsvalError(errstr)
            else:
                sys.exit(1) 
                                
        except GenXmlIfError, errstr:
            print 'IP-XACT error parsing %s :: %s' % (filename, errstr)
            critical( "GenXMLIf Parsing aborted!")
            if self.parent.args.debug:
                raise GenXmlIfError(errstr)
            else:
                sys.exit(1)   
        
        except IOError, errstr:
            if errstr.__str__().find('socket') != -1:
                print 'IP-XACT IOError detected.  IP-XACT validation requires a valid internet \
                connection and access to the schema defined in the IP-XACT document, validation can be disabled with the \'validate_against_schema = False\' flag in an [ipxact] configuration section'
            else:
                print 'IP-XACT IOError error validating %s :: %s' % (filename, errstr)
            critical("Validation aborted!")
            if self.parent.args.debug:
                raise pyxsval.XsvalError(errstr)
            else:
                sys.exit(1) 
                                       
        except ET.ParseError, errstr:
            print 'IP-XACT error parsing %s :: %s' % (filename, errstr)
            critical("ElementTree Parsing aborted!")
            if self.parent.args.debug:
                raise ET.ParseError(errstr)
            else:
                sys.exit(1)

        #Extract namespace and set up 'spirit' template to create namespaces
        self.namespace = self.root.tag[1:].split("}")[0]
        self.spirit = string.Template('{'+self.namespace+'}$tag')
        
        return self
    