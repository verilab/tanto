/*
   Copyright 2012 Verilab, Inc.
   All Rights Reserved Worldwide
*/                                      

// RTL implementation of a parameterized width and depth SYNCHRONOUS FIFO.
// ***NOT SUITABLE FOR USE IN ASYNC APPLICATION***
// Async reset, sync flush, sync read, sync write control inputs, sync data
// input. Sync level, async full, async empty status outputs, async data output.

module fifo #(WIDTH=32, DEPTH=32) (
  input  clk,
  input  rst_n,
  input  flush,
  input  read,
  input  write,
  input  [WIDTH-1:0] d_in,
  output wire [WIDTH-1:0] d_out,
  output reg  [$clog2(DEPTH):0] level,
  output wire full,
  output wire empty,
  output reg  overflow,
  output reg  underflow
);

  reg  qfull;
  reg  qempty;
  reg  [WIDTH-1:0] storage [DEPTH-1:0];
  reg  [$clog2(DEPTH)-1:0] wAddr;
  reg  [$clog2(DEPTH)-1:0] rAddr;
  wire [$clog2(DEPTH)-1:0] wAddrNext;
  wire [$clog2(DEPTH)-1:0] rAddrNext;

  assign wAddrNext = wAddr + 1;
  assign rAddrNext = rAddr + 1;

  always_ff @(posedge clk or negedge rst_n) begin
    if (rst_n == 1'b0) begin
      wAddr <= '0;
      rAddr <= '0;
      qfull <= 1'b0;
      qempty <= 1'b1;
      level <= '0;
      overflow <= 1'b0;
      underflow <= 1'b0;
    end
    else if (flush == 1'b1) begin
      wAddr <= '0;
      rAddr <= '0;
      qfull <= 1'b0;
      qempty <= 1'b1;
      level <= '0;
      overflow <= 1'b0;
      underflow <= 1'b0;
    end
    else begin
      if (write && !(qfull && !read)) begin
        storage[wAddr] <= d_in;
        wAddr <= wAddrNext;
      end
      if (read && !(qempty && !write)) begin
        rAddr <= rAddrNext;
      end
      qfull <= full && !(read && !write);
      qempty <= empty && !(write && !read);
      level <= level + (write && !read && !qfull) - (read && !write && !qempty);
      overflow <= (  (write && !read && qfull)
                  || overflow
                  );
      underflow <= (  (read && !write && qempty)
                   || underflow
                   );
    end
  end

  assign d_out = qempty ? (write ? d_in : '0) : storage[rAddr];

  assign full = (  (write && !read && (wAddrNext == rAddr))
                || (qfull && !(read && !write))
                );
  assign empty = (  (read && !write && (rAddrNext == wAddr))
                 || (qempty && !(write && !read))
                 );

endmodule
