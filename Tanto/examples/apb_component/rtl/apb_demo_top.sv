/*
   Copyright 2012 Verilab, Inc.
   All Rights Reserved Worldwide
*/

// A wrapper module around a APB slave interface, a regfile, and some functional
// blocks (a counter and a fifo) accessed through the regfile.
module apb_demo_top #(ADDR_WIDTH=16, DATA_WIDTH=32) (
  input  rst_n,

  // apb bus.
  input  pclk,
  input  [ADDR_WIDTH-1:0] paddr,
  input  pwrite,
  input  psel,
  input  penable,
  input  [DATA_WIDTH-1:0] pwdata,
  output logic [DATA_WIDTH-1:0] prdata,
  output logic pready,
  output logic pslverr
);

  parameter FIFO0_DEPTH = 16;
  parameter FIFO0_LEVEL_WIDTH = $clog2(FIFO0_DEPTH);

  // Bus interface.
  logic [ADDR_WIDTH-1:0] address;
  logic [DATA_WIDTH-1:0] writeData;
  logic [DATA_WIDTH-1:0] readData;
  apbSlave #(.ADDR_WIDTH(ADDR_WIDTH), .DATA_WIDTH(DATA_WIDTH)) apbSlave (
    .pclk(pclk),
    .paddr(paddr),
    .pwrite(pwrite),
    .psel(psel),
    .penable(penable),
    .pwdata(pwdata),
    .prdata(prdata),
    .pready(pready),
    .pslverr(pslverr),
    .address(address),
    .writeCmd(writeCmd),
    .readCmd(readCmd),
    .writeData(writeData),
    .readData(readData),
    .ack(1'b1),
    .error(1'b0)
  );

  // Register file.
  logic [DATA_WIDTH-1:0] counter0_d_in;
  logic [DATA_WIDTH-1:0] counter0_d_out;
  logic [DATA_WIDTH-1:0] fifo0_d_in;
  logic [DATA_WIDTH-1:0] fifo0_d_out;
  logic [FIFO0_LEVEL_WIDTH:0] fifo0_level;
  apb_demo apb_demo (
    .clk(pclk),
    .rst_n(rst_n),
    .address(address),
    .writeCmd(writeCmd),
    .readCmd(readCmd),
    .writeData(writeData),
    .readData(readData),
    .counter0_control_writeCmd(),
    .counter0_control_readCmd(),
    .counter0_control_start(counter0_start),
    .counter0_control_stop(counter0_stop),
    .counter0_control_reset(counter0_reset),
    .counter0_status_readCmd(),
    .counter0_status_running_readData(counter0_running),
    .counter0_status_paused_readData(counter0_paused),
    .counter0_status_zero_readData(counter0_zero),
    .counter0_count_writeCmd(counter0_ld),
    .counter0_count_readCmd(),
    .counter0_count_value_writeData(counter0_d_in),
    .counter0_count_value_readData(counter0_d_out),
    .fifo0_control_writeCmd(),
    .fifo0_control_readCmd(),
    .fifo0_control_flush(fifo0_flush),
    .fifo0_status_readCmd(),
    .fifo0_status_fifo_empty_readData(fifo0_empty),
    .fifo0_status_fifo_full_readData(fifo0_full),
    .fifo0_status_fifo_level_readData({{7-FIFO0_LEVEL_WIDTH{1'b0}}, fifo0_level}),
    .fifo0_status_fifo_overflow_readData(fifo0_overflow),
    .fifo0_status_fifo_underflow_readData(fifo0_underflow),
    .fifo0_data_in_writeCmd(fifo0_write),
    .fifo0_data_in_data_in_writeData(fifo0_d_in),
    .fifo0_data_out_readCmd(fifo0_read),
    .fifo0_data_out_data_out_readData(fifo0_d_out)
  );

  // counter0 glue logic.
  assign counter0_running = counter0_start && !counter0_stop && !counter0_reset;
  assign counter0_paused = !(counter0_start && !counter0_stop) && !counter0_reset;
  assign counter0_zero = (counter0_d_out == 'h0);

  counter #(.WIDTH(DATA_WIDTH)) counter0 (
    .clk(pclk),
    .rst_n(rst_n),
    .clr(counter0_reset),
    .ld(counter0_ld),
    .ena(counter0_running),
    .d_in(counter0_d_in),
    .d_out(counter0_d_out)
  );

  fifo #(.WIDTH(DATA_WIDTH), .DEPTH(FIFO0_DEPTH)) fifo0 (
    .clk(pclk),
    .rst_n(rst_n),
    .flush(fifo0_flush),
    .read(fifo0_read),
    .write(fifo0_write),
    .d_in(fifo0_d_in),
    .d_out(fifo0_d_out),
    .level(fifo0_level),
    .full(fifo0_full),
    .empty(fifo0_empty),
    .overflow(fifo0_overflow),
    .underflow(fifo0_underflow)
  );

endmodule
