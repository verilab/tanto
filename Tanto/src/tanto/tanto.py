'''
The main management class. Read IP-XACT, render with the template engines.

Created on Jun 27, 2012

Copyright 2012 Verilab, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
   
@author: gmcgregor
'''
from logging import info, debug, warning
import getpass
import argparse
import logging
import os.path
import pprint
import yaml
import ConfigParser
from mako.lookup import TemplateLookup     

import tanto
from tanto_renderer import TantoRenderer
from tanto_transformer import TantoTransformer
from tanto_parser import TantoParser

__version__ = '0.9a'

class Tanto(object): 
    '''The main class.  Handles delegating IP-XACT parsing and template rendering'''
       
    def __init__(self):
        '''Constructor - mostly harmless'''
        pass
    
    def load_config(self):
        '''Process command line, parse configuration, setup template paths'''
        self.args = self.process_command_line()

        if self.args.debug:
            logging.basicConfig(level = logging.DEBUG)
            
        if self.args.verbose:
            logging.basicConfig(level = logging.INFO)    
            
        self.config = ConfigParser.SafeConfigParser()
        info("Parsing %s for project configuration file" % self.args.config)
        self.config.read(self.args.config)
        
        self.add_parser_config()
        
        # Read the project configuration to get the location of the template directory
        # Then iterate over the selected templates and load any optional <template>.ini files
        # and finally re-parse the original project config so that any project overrides
        # to the template ini files will take priority
        for template in self.args.templates:
            self.update_config(template)
        self.config.read(self.args.config)

        self.template_lookup = TemplateLookup(directories=self.config.get('tanto','template_directory'), module_directory='/tmp/%s-mako/modules' % getpass.getuser())
        debug(self.args)
     
    def update_config(self, template):
        try:
            template_ini = os.path.join(self.config.get('tanto', 'template_directory'), template, '%s.ini' % template )
            debug("Trying to load %s" % template_ini)
            self.config.read(template_ini)
            debug("Success")
        except IOError:
            pass
       
    def get_parser(self):
        return self.get_delegate(self.args.parser, 'parsers', '_parser', 'Parser' )
        
    def get_transformer(self):
        return self.get_delegate(self.args.transformer, 'transformers', '_transformer', 'Transformer' )
 
    def get_renderer(self, name):
        return self.get_delegate(name, 'templates', '_renderer', 'Renderer' )
        
    def get_delegate(self, name, directory, extension, classtype):
        classname = '%s%s' % (name.capitalize(), classtype)
        import_string = 'from %s.%s.%s%s import %s' % (directory, name, name, extension, classname)
        debug(import_string)
        exec import_string
        delegate = locals()[classname](self)
        debug('Using delegate %s' % classname)        
        return delegate
            
    def add_parser_config(self):
        import_string = 'import parsers.%s' % (self.args.parser)
        exec import_string
        parser_path = eval ('parsers.%s.__file__' % self.args.parser)
        parser_ini = os.path.join(os.path.dirname(parser_path), '%s.ini' % self.args.parser)        
        try:
            self.config.read(parser_ini)
        except IOError:
            pass
        
    def parse(self):
        '''Go find the parser and delegate parsing'''
        parser = self.get_parser()
        self.parsed_data = parser.parse()
        
    def transform(self):
        '''Go find the transformer and delegate the transform'''
        transformer = self.get_transformer()
        self.data = transformer.transform(self.parsed_data)
                  
    def process_command_line(self):
        '''Command-line option parsing'''
        arg_parser = argparse.ArgumentParser(description=('Tanto IP-XACT Parser v%s' % tanto.__version__))
        arg_parser.add_argument('-d', '--debug', action='store_true')
        arg_parser.add_argument('-v', '--verbose', action='store_true')
        arg_parser.add_argument('-c', '--config', default = './config.ini')
        arg_parser.add_argument('-p', '--parser', default = 'ipxact')
        arg_parser.add_argument('-t', '--transformer', default = 'ipxact')
        arg_parser.add_argument('templates', metavar='template', nargs='+', help='Template(s) to generate')
        return arg_parser.parse_args()
        
    def render(self):     
        '''Assume we now have a register model, go through each template defined on the command line
           and render the associated view, saving to the indicated file(s)''' 

        for template in self.args.templates:
            try:                
                renderer = self.get_renderer(template)
            except ImportError:
                renderer = TantoRenderer(self)              
            renderer.render(template)

            
    def run(self):
        self.load_config()
        debug('starting parse')
        self.parse()
        debug('starting transform')
        self.transform()
        debug('starting render')
        self.render()
            